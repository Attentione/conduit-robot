*** Settings ***
Library    Browser
Library    String
Library    read_data.py


*** Variables ***
#${BROWSER}              chromium
#${BROWSER}              firefox
#${URL}                  https://karhukaiset.devopskoulutus.net/
#${URL}                  https://karhukaiset-dev.devopskoulutus.net
${NAME_PREFIX}           testibotti                    
${EMAIL_SUFFIX}          @testi.fi
${EMAIL}                 Actual value comes from Generate Name keyword
${NAME}                  Actual value comes from Generate Name keyword
${TEXT}                  Kissa
${TEXT1}                 Kissat
${TEXT2}                 Kissat on parhaita





*** Keywords ***
Generate Name
    ${NUMBER} =           Generate Random String  length=6  chars=[NUMBERS]          
    ${NAME} =             Catenate    ${NAME_PREFIX}${NUMBER} 
    Set Suite Variable    ${NAME}
    ${EMAIL} =            Catenate    ${NAME}${EMAIL_SUFFIX}
    Set Suite Variable    ${EMAIL}    
    Log To Console        \n${NAME}
    Log To Console        ${EMAIL}

Initialize Browser
    New Browser           ${BROWSER}     headless=false
    New Context           viewport={'width': 1920, 'height': 1080}
    New Page              ${URL}
    Get Title             ==    Conduit
    #sleep                 1

Sign up
    Click                "Sign up"
    Type Text            css=[placeholder="Username"]    ${NAME} 
    Type Text            css=[placeholder="Email"]       ${EMAIL}
    Type Text            css=[placeholder="Password"]    ${NAME}
    Keyboard Key         press    Enter
    #sleep                1

Log out
    Click                css=[href="/settings"]
    Click                "Or click here to logout."
    #sleep                1

Sign In
    Click                "Sign in"
    Type Text            css=[placeholder="Email"]       ${EMAIL}
    Type Text            css=[placeholder="Password"]    ${NAME}
    Keyboard Key         press    Enter
    #sleep                1

New Post
    Click                css=[href="/editor"]
    Type Text            css=[placeholder="Enter tags"]                                ${TEXT}
    Keyboard Key         press    Enter            
    Type Text            css=[placeholder="Article title (obligatory field and max. 200 characters)"]                             ${TEXT}
    Type Text            css=[placeholder="Article description (obligatory field and max. 500 characters)"]                ${TEXT1}
    Type Text            css=[placeholder="Write your article (in markdown)"]          ${TEXT2}
    Fill Text            css=[placeholder="Enter tags"]                                ${TEXT}
    Click                "Publish Article"
    #sleep                1

New Postfromlist
    Click                css=[href="/editor"]   
    Type Text            css=[placeholder="Article title (obligatory field and max. 200 characters)"]                      ${TEXT}
    Type Text            css=[placeholder="Write your article (in markdown)"]          ${LIST}
    Click                "Publish Article"
    #sleep                1

Generate data             #Data from https://gitlab.com/madc0w/big-list-of-naughty-strings/-/blob/master/blns.txt
    @{LIST}=            read_data
    set suite variable  @{LIST}   

Loop data
    Initialize Browser    
    FOR    ${i}    IN RANGE     10
        Generate Name
        Sign up
        New Post
        Log out
    END
    
Loop LIST            
    Initialize Browser
    Generate data
    Generate Name
    Sign up
        FOR    ${list}     IN    @{LIST}
            Click                css=[href="/editor"]
            Type Text            css=[placeholder="Enter tags"]                                "naughty"
            Keyboard Key         press    Enter            
            Type Text            css=[placeholder="Article title (obligatory field and max. 200 characters)"]                      ${list}
            Type Text            css=[placeholder="Write your article (in markdown)"]          ${list}
            Type Text            css=[placeholder="Article description (obligatory field and max. 500 characters)"]                ${list}
            Click                "Publish Article"   
            Click                css=[href="/editor"]
        END
        


Wait
    sleep            3