# Requirements


## Robotframeworks

- pip install robotframework

## Robotframework browserlibrary:

Installation instructions
Only Python 3.7 or newer is supported.


- Install node.js e.g. from https://nodejs.org/en/download/
- Install robotframework-browser from the commandline: **pip install robotframework-browser**
- Install the node dependencies: run **rfbrowser init** in your shell